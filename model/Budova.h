#ifndef BUDOVA_H
#define BUDOVA_H

#include "iostream"
using namespace std;

class Budova {

private:
    int m_pocetObyvatel;
    int m_maxPocetObyvatel;
    int m_csp;

public:
    Budova(int pocetObyvatel, int maxPocetObyvatel, int csp);
    ~Budova();

    void setPocetObyvatel(int pocet);

    int getPocetObyvatel();

    int getMaxPocetObyvatel();

    int getCSP();

    void printInfo();
};

#endif
