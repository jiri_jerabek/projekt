#include "Firma.h"

Firma::Firma(string nazev, int pocetZamestnancu, int vklad)
{
    m_nazev = nazev;
    m_pocetZamestnancu = pocetZamestnancu;
    m_penize = vklad;
}

int Firma::getPocetZamestnancu() {
    return m_pocetZamestnancu;
}

void Firma::naberZamestnance(int pocet) {
    m_pocetZamestnancu += pocet;
}

void Firma::propustZamestnance(int pocet) {
    m_pocetZamestnancu -= pocet;
}

void Firma::printInfo() {
    cout << "--Firma--" << endl
         << "Nazev: " << m_nazev << endl
         << "Pocet zamestnancu: " << m_pocetZamestnancu << endl
         << "Penize firmy: " << m_penize << endl
         << "---------" << endl;
}
