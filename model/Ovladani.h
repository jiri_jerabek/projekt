#ifndef OVLADANI_H
#define OVLADANI_H

#include "iostream"
using namespace std;

class Ovladani
{
public:
  Ovladani();
  static int start();

  static int menu();
};

#endif // OVLADANI_H
