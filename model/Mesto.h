#ifndef MESTO_H
#define MESTO_H

#include "Budova.h"
#include "Firma.h"

#include "iostream"

using namespace std;

class Mesto {

private:
    string m_nazev;
	int m_pocetObyvatel;
    int m_maxPocetObyvatel;
	int m_pocetBudov;
    int m_pocetZamestnancu;
    int m_pocetFirem;


public:
    Mesto(string nazev);

	void postavBudovu(Budova* budova);

	void demolujBudovu(Budova* budova);

    void postavFirmu(Firma* firma);

    void zrusFirmu(Firma* firma);

    void printInfo();

    string getNazev();
};

#endif
