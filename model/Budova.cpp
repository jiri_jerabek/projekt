#include "Budova.h"

Budova::Budova(int pocetObyvatel, int maxPocetObyvatel, int csp) {
    m_pocetObyvatel = pocetObyvatel;
    m_maxPocetObyvatel = maxPocetObyvatel;
    m_csp = csp;
}
Budova::~Budova(){
   m_pocetObyvatel = 0;
}

void Budova::setPocetObyvatel(int pocet) {
    m_pocetObyvatel = pocet;
}

int Budova::getPocetObyvatel() {
    return m_pocetObyvatel;
}

int Budova::getMaxPocetObyvatel() {
    return m_maxPocetObyvatel;
}

int Budova::getCSP() {
    return m_csp;
}

void Budova::printInfo() {
    cout << "--Budova--" << endl
         << "CSP: " << m_csp << endl
         << "Pocet obyvatel: " << m_pocetObyvatel << " / " << m_maxPocetObyvatel << endl
         << "----------" << endl;
}


