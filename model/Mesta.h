#ifndef MESTA_H
#define MESTA_H

#include <vector>
#include <iostream>
#include "Mesto.h"

using namespace std;

class Mesta
{
   private:
      static vector<Mesto*> m_mesta;
public:
    Mesta();

     static void pridejMesto(Mesto* mesto);

     static void printInfo();
};

#endif // MESTA_H
