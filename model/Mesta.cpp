#include "Mesta.h"

#include <vector>
#include <iostream>
#include "Mesto.h"

using namespace std;

vector<Mesto*> Mesta::m_mesta;

Mesta::Mesta()
{

}

 void Mesta::pridejMesto(Mesto* mesto)
{
  m_mesta.push_back(mesto);

}

void Mesta::printInfo()
{
    cout << "-------MESTA--------" << endl;
    for(int i=0;i<m_mesta.size();i++)
    {
      m_mesta.at(i)->printInfo();
    }

}
