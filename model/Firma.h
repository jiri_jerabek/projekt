#ifndef FIRMA_H
#define FIRMA_H

#include "iostream"
using namespace std;

class Firma
{
private:
    string m_nazev;
    int m_pocetZamestnancu;
    int m_penize;
    int m_poceFirem;

public:
    Firma(string nazev,int pocetZamestnancu, int vklad);

    int getPocetZamestnancu();

    void naberZamestnance(int pocet);

    void propustZamestnance(int pocet);

    void printInfo();

};

#endif // FIRMA_H
