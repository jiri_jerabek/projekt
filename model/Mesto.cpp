#include "Mesto.h"
#include "Budova.h"
#include "Firma.h"

Mesto::Mesto(string nazev) {
    m_nazev = nazev;
    m_pocetBudov = 0;
    m_pocetObyvatel = 0;
    m_maxPocetObyvatel = 0;
    m_pocetZamestnancu = 0;
    m_pocetFirem = 0;
}

void Mesto::postavBudovu(Budova* budova) {
   m_pocetBudov++;
   m_pocetObyvatel += budova->getPocetObyvatel();
   m_maxPocetObyvatel += budova->getMaxPocetObyvatel();
}

void Mesto::demolujBudovu(Budova* budova) {
    m_pocetObyvatel -= budova->getPocetObyvatel();
    delete budova;
    m_pocetBudov--;
}

void Mesto::postavFirmu(Firma* firma) {
    m_pocetZamestnancu += firma->getPocetZamestnancu();
    m_pocetFirem++;
}

void Mesto::zrusFirmu(Firma* firma) {
    m_pocetFirem--;
}

string Mesto::getNazev()
{
    return m_nazev;
}

void Mesto::printInfo() {
    cout << "--Mesto--" << endl
         << "Nazev: " << m_nazev << endl
         << "Pocet budov: " << m_pocetBudov << endl
         << "Obyvatel: " << m_pocetObyvatel << " / " << m_maxPocetObyvatel << endl
         << "Pocet zamestnancu: " << m_pocetZamestnancu << endl
         << "---------" << endl;
}

